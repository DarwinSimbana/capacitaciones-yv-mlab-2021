# JavaScript
## MENU
- [ir al Menu General](https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/edit/master/doc.MD)
- [¿Que es Javascript?](#javascript)
- [lenguaje para web](#web)
- [No tipado](#tipado)
- [DOM](#DOM)
- [ECMAScript](#ECMAScript)
- [Back&front](#Back&front)
- [Interpretado](#Interpretado)

## Javascript
es un lenguaje de programación interpretado, dialecto del estándar ECMAScript Se basa en prototipos, imperativo, débilmente tipado y dinámico y es implementado como parte de un navegador web.  
<img src="" width=""/>  

## Web
se puede aplicar a un documento HTML y usarse para crear interactividad dinámica en los sitios web.   
Es un lenguaje de programación o de secuencias de comandos que te permite implementar funciones complejas en páginas web.
<img src="" width=""/>  

## Tipado
javascript no es ta tipado o es debilmente tipado, significa que las variables son declaradas sin un tipo, valores pueden modificarse, compararse y operar entre ellos sin necesidad de realizar una conversión previa.  
<img src="" width=""/>  
A diferencia de los Los denominados fuertemente tipados corresponden con aquellos lenguajes que no permiten comparar u operar con tipos de datos distintos sin realizar una conversión previa; como Java o Python.  

## DOM
### ¿Que es?
Las siglas DOM significan Document Object Model, o lo que es lo mismo, la estructura del documento HTML. Una página HTML está formada por múltiples etiquetas HTML, anidadas una dentro de otra, formando un árbol de etiquetas relacionadas entre sí, que se denomina árbol DOM (o simplemente DOM).  
### DOM en Javascript
En Javascript, cuando nos referimos al DOM nos referimos a esta estructura, que podemos modificar de forma dinámica desde Javascript, añadiendo nuevas etiquetas, modificando o eliminando otras, cambiando sus atributos HTML, añadiendo clases, cambiando el contenido de texto entre otros.  
<img src="" width=""/>  

## ECMAScript
Se define un lenguaje de tipos dinámicos ligeramente inspirado en Java y otros lenguajes del estilo de C. Soporta algunas características de la programación orientada a objetos mediante objetos basados en prototipos y pseudoclases.  
### Javascript
La mayoría de navegadores de Internet incluyen una implementación del estándar ECMAScript, al igual que un acceso al Document Object Model para manipular páginas web. JavaScript está implementado en la mayoría de navegadores.  
<img src="" width=""/>  

## Back&front
el **Back-End** es la parte o rama del desarrollo web encargada de que toda la lógica de una página funcione.  

el **Front-End** está encargado de implementar todo lo que se relaciona con la parte visible de una web, con lo que el usuario entra en contacto al navegar por la página.  

### JavaScript
javascript gracias a que interectua y se combina con **CSS** y **HTML** se puede trabajar facilmente en la parte logica y visual de la aplicacion Web.  
<img src="" width=""/>  

## Interpretado
JavaScript es un **lenguaje interpretado** línea a línea por el navegador, mientras se carga la página, que solamente es capaz de realizar las acciones programadas en el entorno de esa página HTML donde reside. Sólo es posible utilizarlo con otro programa que sea capaz de interpretarlo, como los navegadores web.
<img src="" width=""/>  



