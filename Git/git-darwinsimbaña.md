# Git
## MENU
- [ir al Menu General](https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/edit/master/doc.MD)
- [¿Que es Git?](#git)
- [Comandos git en consola](#consola)
- [Clientes Git](#clientes)
- [Clonación por cliente y consola](#clonación)
- [Commits por cliente y consola](#commits)
- [Ramas desde kraken](#ramas)
- [Merge](#merge)

# Git
Es un software de control de versiones, pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando estas tienen un gran número de archivos de código fuente.

# Consola
| Comandos | Función| 
|---------|--------|
|git init| Esto crea un subdirectorio nuevo llamado. git, el cual contiene todos los archivos necesarios del repositorio un esqueleto de un repositorio de Git.Todavía no hay nada en tu proyecto que esté bajo seguimiento|
|git fetch|Descarga los cambios realizados en el repositorio remoto.
|git merge <nombre_rama>|Impacta en la rama en la que te encuentras parado, los cambios realizados en la rama "nombre_rama".|
|git pull|Unifica los comandos fetch y merge en un único comando.|
|git commit -m "mensaje"|Confirma los cambios realizados. El “mensaje” generalmente se usa para asociar al commit una breve descripción de los cambios realizados.|
|git push origin "nombre_rama"|Sube la rama “nombre_rama” al servidor remoto.|
|git status|Muestra el estado actual de la rama, como los cambios que hay sin commitear.|
|git add <nombre_archivo>|Comienza a trackear el archivo “nombre_archivo”.|
|git checkout -b <nombre_rama_nueva>|Crea una rama a partir de la que te encuentres parado con el nombre “nombre_rama_nueva”, y luego salta sobre la rama nueva, por lo que quedas parado en esta última.|
|git checkout -t origin/<nombre_rama>|Si existe una rama remota de nombre “nombre_rama”, al ejecutar este comando se crea una rama local con el nombre “nombre_rama” para hacer un seguimiento de la rama remota con el mismo nombre.|
|git branch|Lista todas las ramas locales.|
|git branch -a:|Lista todas las ramas locales y remotas.|
|git branch -d <nombre_rama>|Elimina la rama local con el nombre “nombre_rama”.|
|git push origin <nombre_rama>| Commitea los cambios desde el branch local origin al branch “nombre_rama”.|
|git remote prune origin|Actualiza tu repositorio remoto en caso que algún otro desarrollador haya eliminado alguna rama remota.|
|git reset --hard HEAD|Elimina los cambios realizados que aún no se hayan hecho commit.|
|git revert <hash_commit>|Revierte el commit realizado, identificado por el “hash_commit”.|

# Clientes
Los clientes de la interfaz gráfica de usuario (GUI, por sus siglas en inglés) son herramientas que proporcionan una visualización alternativa para Git.
## Git Kraken
Git kraken es un cliente multiplataforma, es confiable, eficiente, Su interfaz es intuitiva, ya que permite a los usuarios realizar rápidamente acciones básicas y tiene una función de arrastrar y soltar. es confiable, eficiente, visualmente agradable y elegante de usar, sino que también hace que las operaciones de git sean comprensibles y agradables. Su interfaz es intuitiva, ya que permite a los usuarios realizar rápidamente acciones básicas y tiene una función de arrastrar y soltar  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/logoGitkraken.jpg" width="" align="center"/>

# Clonación
## Cliente Git kraken
1. La clonacion en kraken es mas sencilla gracias a su interfaz grafica primero tenemos que tener una cuenta asociada en este caso es gitlab crear un unevo proyecto elegimos clonar.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/clonacionKraken.png" width=""/>

2. despues elegimos la cuenta en donde se encuentra el repositorio.
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/clonacionKraken2.png" width=""/>   
 
3. ahi elegimos el direrctorio donde se va a clonar el nombre del repositorio y el nombre como se va a guardar la clonacion.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/clonacionKraken3.png" width=""/> 

4. luego solo damos en clic en clonar y listo tendremos la clonacion realizada con exito.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/clonacionKraken4.png" width="1000"/>  

## Consola
1. ejecutamos el comando git clone seguido de la direccion del repositorio.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/clonacion.png" width=""/>  

2. luego tendriamos que autenticar las credenciales del nombre de cuenta y contraseña.finalmente nos apareceria un mensaje que la clonacion fue exitosa.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/consola.jpg.png" width=""/> 


# Commits
## commits clientes

1. los comits por cliente se realizan por medio de un interfaz grafica en este caso usaremos gitkraken, primero añadiremos el archivo para realizar el commit siguiente confirmamos el commit t listo. solo tendriamos que relzair el push para subir el cambio al repositorio.
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/commitKraken/commitkraken2.png" width="200"/>  

## Commits por consola

1. primero ejecutaremos el comando git status para saber los archivos exitentes.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/comitts/comitt1.png" width=""/>  

2. ejecutamos el comando git add donde añadiremos el archivo para el commit.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/comitts/comitt2.png" width=""/>  

3. se nos añadira y precederemos a realizar el commmit. 
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/comitts/comitt3.png" width=""/>  

4. con el comnado status verficaremos si esta listo el commit y luego solo con el comando push subiremos el commit y el archivo al repositorio.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/comitts/comitt4.png" width=""/>  

# Ramas
al momento de un proyecti en kraken podremos ver nuestro proyecto completo, ramas, commits, etc. Y acada rama en kraken se crea de acuerdo al al ma creada o relizada por un nuevo participante.
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/ramasKraken/ramasKraken2.png" width=""/>  


# Merge
En git existen dos formas que nos permiten unir ramas, git merge y git rebase. La forma mas conocida es git merge, la cual realiza una fusión a tres bandas entre las dos últimas instantáneas de cada rama y el ancestro común a ambas, creando un nuevo commit con los cambios mezclados.
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/mergue.png" width=""/>  

Git básicamente lo que hace es recopilar uno a uno los cambios confirmados en una rama, y reaplicarlos sobre otra. Utilizar rebase nos puede ayudar a evitar conflictos siempre que se aplique sobre commits que están en local y no han sido subidos a ningún repositorio remoto. Si no tienen cuidado con esto último y algún compañero utiliza cambios afectados, seguro que tendrá problemas ya que este tipo de conflictos normalmente son difíciles de reparar.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-imagenes/mergue2.png" width=""/>  

- [volver al menu](#menu)
