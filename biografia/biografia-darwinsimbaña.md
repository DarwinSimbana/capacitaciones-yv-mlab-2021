# Biografia

## MENU
- [ir al Menu General](https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/edit/master/doc.MD)
- [indice general](documentacion/doc.md)
- [Informacion general](#datos)
- [Aspiraciones](#apiraciones)
- [Pasatiempos](#pasatiempos)

<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/fotoperfil/fotoperfil.jpg" width="100"/>

## Datos
Mi nombre es Darwin Genaro Simbaña Oña, tengo 19 años de edad, naci el 31 de octubre del 2001, en este momento vivo en el barrio San Jose de Cangahua en la cuidad de Quito con mi mamá Maria Martha Oña, con mi papá Segundo Simbaña soy el cuarto hijo de cinco, tengo tres hermanos y una hermana. Actualmente estoy estudiando la carrera de desarrollo de software en el instituto Benito Juarez cursando el segundo semestre. En mi vida escolar estudie en la escuela Tarquino Idrovo al terminar la escuela estudie en el colegio Patrimonio de la Humanidad, donde me gradue.

## Apiraciones
A lo largo del tiempo he aprendido muchas cosas, conocido muchas personas y aun me queda mucho por delante y mucho mas que aprender. Mi actual objetivo es graduarme de mi actual carrera, se quizas haya momentos complicados pero se que lo lograre.

## Pasatiempos
mis hobbies o pasatiempos, mayormente cuando tengo tiempo libre lo ocupo en ver series o jugar videojuegos

- [Volver al menu](#menu)
