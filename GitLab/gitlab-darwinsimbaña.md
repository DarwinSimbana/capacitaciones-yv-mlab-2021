# GitLab
# MENU
- [ir al Menu General](https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/edit/master/doc.MD)
- [¿Que es GitLab?](#gitlab)
- [¿Como accedo a GitLab?](#acceder)
- [Crear repositorios](#repositorios)
- [Crear grupos](#grupos)
- [subgrupos](#subgrupos)
- [Crear issues](#issues)
- [Crear labels](#labels)
- [Agregar miembros](#agregar)
- [Crear boards](#boards)
- [Crear committs](#committs)
- [Permisos ¿cuales hay? y ¿para que sirven?](#permisos)


## GitLab
Es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto.

## acceder
GitLab es una herramienta basada en Git, así que funciona de la misma manera que otras herramientas similares. Generalmente usa comandos, o a través de programas de interfaz gráfica, o del propio editor de código.
GitLab ofrece hosting remoto para repositorios, interfaz web para controlar el repositorio, examinar el código en cualquiera de sus versiones, mergear el código de versiones de proyecto o gestionar las "pull request" (que en GitLab se llaman "merge request"), gestionar problemática de tu software, automatizar procesos como el despliegue o la ejecución de pruebas del software, etc.

## repositorios

1. para crear repositorios en gitla primero debemos dirigirnos donde tenemos todos nuestros repositiorios y en la parte superior derecha encontraremos un boton para crer un repositorio nuevo, damos clic.   
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/repositorio/repositorio.png" width=""/>

2. elegimos la opcion acorde a lo que necesitemos en este caso elegirermos la opcion de crear en blanco.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/repositorio/repositorio2.png" width="700"/>  

3. proecederemos a configurar nuestro repositorio. como el nombre, si es publico o privado y lasmas importante debemos marcar la casilla de laultima opcion. entre otras configuraciones.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/repositorio/repositorio3.png" width="700"/>  

4. damos a crear y listo tendremos el repositorio creado.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/repositorio/repositorio4.png" width="700"/>  

## grupos
1. primero nos dirigiremos al menu principal de la parte superior.   
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/grupos/grupos1.png" width=""/>  

2. damos clic y escogeremos crear grupo.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/grupos/grupos2.png" width=""/>  

3. configuramos el grupo e invitamos a los participantes por medio del correo  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/grupos/grupos3.png" width="500"/>  

4. damos clic en crear y ya tendremos el grupo creado.    
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/grupos/grupos5.png" width="700"/>

## subgrupos
1. en el grupo creado tenemos una opcion que nos permite crear un subgrupo le damos clic.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/subgrupos/subgrupos.png" width=""/>  

2. configuramos el grupo y le damos a crear y ya tenemos un subgrupo creado.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/subgrupos/subgrupos1.png" width=""/>

## issues
1. para crear un issu nos dirigiremos en el repositorioa la opcion de issues donde si no tenemos ninguno seleccionaremos crear o en tal caso de necesitarlo la opcion de exportar uno.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/issues/issues.png" width="700"/>  

2. luego tendremos la pantalla para empezar a crear un issue y configurarlo, el issue para su creacion utiliza el lenguaje Markdown.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/issues/issues2.png" width="700"/>  

## labels
1. primero en la opcion de issues elegiremos la opcion la labels.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/labels/labels1.png" width=""/>  

2. luego damos clic a labels se nos abria para un pantalla para crear un label nuevo damos clic.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/labels/labels2.png" width="700"/>  

3. luego solo confguraremos o crearemos el label de acuerdo a lo necesitado.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/labels/labels3.png" width="700"/>  

## Agregar
1. para agregar un miembro a un grupo debemos ir a un repositorio y enlas opciones elegir la opcion miembros damos clic.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/miembros/miembros1.png" width=""/>  

2. se abrira una pantalla para realiazr la invitacion a un participante. como el permiso asignado a la persona y si la invitacion tiene  expiracion.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/miembros/miembros2.png" width="700"/>  

## boards
1. para crear un board debemos dirigirnos al apartado de issues donde encontremos la opcion de boards.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/boards/boards.png" width=""/>  

2. miraremos una pantalla donde en laparte superior derecha podremos crear y editar un board.  
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/boards/boards2.png" width="800"/>   

## Committs
para realizar un committ en GitLab debemos modificar un archivo dentro del repositorio y al momento de guardar los cambios debemos llenar el mensaje enviado al ejecutar el committ.  
Además se puede encontar la ramas que tendra es decir el nombre de la rama en el cual se realizo el comitt
<img src="https://gitlab.com/DarwinSimbana/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab-imagenes/commitsGitlab/commits.png" width=""/>  

## Permisos
### ¿cuales hay? y ¿para que sirven?
existen cuatro permisos: Guest, Reporter, Developer, y Master cada tiene acciones limitadas al momento de interactuar con un repositorio es decir tiene acciones limitadas dentro de el.
|Guest|Reporter|Developer|Master|
|-----|--------|---------|------|
|Crear un nuevo problema|	Capaz de extraer el codigo del proyectot|Cree un nueva solicitud de combinacion|	Puede agregar nuevos miembros al equipo|
|Capaz de escribir en el muro del proyecto|Puede descargar el proyecto|Capaz de escribir fragmentos de codigo|	Insertar y eliminar ramasprotegido|

el rol guest tiene pocos permisos comparado con el master que ademas de terer los permisos de el tiene todos los demas, asi se van dilimitando permisos.
- [ir a menu](#menu)
